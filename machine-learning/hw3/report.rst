***************************
機器學習 作業三
***************************

資工三 403410034 黃鈺程

##########################
Convergence & Overfitting
##########################

model 是有收斂的，overfitting 也不明顯。我想是因為 model 裡有 dropout layer 的關係。

#########################
Experimental Result
#########################

model 收斂得很快，約 5 個 epoch。以下是 10 個 epoch 的圖

.. image:: https://imgur.com/GJ5guS2.png

.. image:: https://imgur.com/AigXFiv.png

與前面的二層 FC 的 model 比較，效果是相當的，正確率都在九成（以上）。而 KNN 的正確率是約六成。從 model 的複雜度來看，選擇二層 FC 的 model 是比較好的，參數少，收斂快。有 Convolution Layer 的 model train 起來真的慢。

#########################
Difficulty
#########################

要計得預處理（標準化 Standardization）資料啊！

在使用 tensorflow 時，常感到自己的智商不夠。所幸 tensorflow 在四月時已經將 Keras 的 API 整合進去了，讓我可以快速地、有效率地使用 Keras 將 model 建起來。

最後繪圖使用 pandas 也非常好用，畫出來的圖很漂亮。不用為了使用 tensorboard，得自己 log 的資料，相對麻煩許多。

########################
Disscusion
########################

許多人會覺得 Keras 靈活性不夠，我一直覺得很奇怪。如果他們指的是想使用沒有內建的 layers，在 Keras 中你也可以自己創 layers 出來啊，方法非常簡單。如果是指 model 會繞來繞去，Sequential Model 做不出六，在 Keras 你也以使用 Functionial Model，我猜他們根本沒看完 Keras 的官方文檔…
