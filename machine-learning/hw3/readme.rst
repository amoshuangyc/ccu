***************************
機器學習 作業三
***************************

資工三 403410034 黃鈺程

###########################
Execusion
###########################

使用 Tensorflow + Jupyter + Pandas。

如果想執行程式，請先將 Jupyter 架好，開啟 ``hw3.ipynb``，設定好 dirname。之後依序執行所有 cells。

如果你只有想瀏灠程式，可以到 `我的 Github`_ 看。Github 可以渲染 Jupyter(``.ipynb``) 格式的檔案。

.. _我的 Github: https://github.com/amoshyc/ccu/blob/master/machine-learning/hw3/hw3.ipynb
