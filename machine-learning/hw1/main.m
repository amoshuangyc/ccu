clear all;

tic;

H = 192;
W = 168;
n_person = 38;
n_train = 35;

X_train = [];
y_train = [];
X_test = [];
y_test = [];

for i = 1:n_person
    dir_name = sprintf('./CroppedYale/yaleB%02d/',  i + (i >= 14) * 1);
    filenames = dir(dir_name);

    imgs = [];
    label = [];
    for j = 1:numel(filenames)
        fn = filenames(j).name;
        if numel(fn) < 24 || ~isempty(strfind(fn, '.bad'))
            continue
        end

        path_name = strcat(dir_name, fn);
        imgs(:, end + 1) = reshape(imread(path_name), [], 1);
        label(end + 1) = i;
    end

    indices = randperm(size(imgs, 2));
    indices_train = indices(1:n_train);
    indices_test = indices(n_train+1:end);

    X_train = [X_train imgs(:, indices_train)];
    X_test = [X_test imgs(:, indices_test)];
    y_train = [y_train label(indices_train)];
    y_test = [y_test label(indices_test)];
end

n_train_img = size(X_train, 2);
n_test_img = size(X_test, 2);

res_sad = [];
res_ssd = [];
for i = 1:n_test_img
    sad = zeros(n_train_img, 1);
    ssd = zeros(n_train_img, 1);
    for j = 1:n_train_img
        sad(j) = norm(X_train(:, j) - X_test(:, i), 1);
        ssd(j) = norm(X_train(:, j) - X_test(:, i), 2);
    end
    
    [~, idx1] = min(sad);
    [~, idx2] = min(ssd);
    res_sad(i) = y_train(idx1);
    res_ssd(i) = y_train(idx2);
    
    disp(i);
end

acc_sad = mean(res_sad == y_test);
acc_ssd = mean(res_ssd == y_test);
fprintf('sad: %.3f%%\n', acc_sad * 100.0);
fprintf('ssd: %.3f%%\n', acc_ssd * 100.0);

toc