# Assignment 1

## Usage

1. Change Dataset path by modifying line 16 in `main.m` (default to `./CroppedYale/`)
2. Run `main.m`
3. Wait roughly 400 seconds (it depends on your cpu).

The number displayed while executing is the number of images finished computing.
According to the experiments performed locally, the accuracy is between 60% and 85%.

## Developing Environment

- Matlab 2016a
- Fedora 25
- i5-6200U (no gpu)

## Email

amoshuangyc@gmail.com