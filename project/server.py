import base64
import cv2
from flask import Flask, request, render_template, jsonify

app = Flask(__name__)

face_cascade = cv2.CascadeClassifier('./haarcascade_frontalface_default.xml')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/getface', methods=['POST'])
def getface():
    _, b64 = request.form['base64img'].split(',')
    with open('./temp.png', 'wb') as f:
        f.write(base64.decodebytes(b64.encode('utf-8')))

    img = cv2.imread('./temp.png')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    rect = None
    if len(faces) > 0:
        (x, y, w, h) = faces[0]
        rect = { 'x': int(x), 'y': int(y), 'w': int(w), 'h': int(h) }

    return jsonify(rect=rect)

if __name__ == '__main__':
    app.run()
