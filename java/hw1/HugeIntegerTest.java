import java.math.BigInteger;
import java.util.*;

public class HugeIntegerTest extends Object {
    public static void main(String[] args) {
        Random rand = new Random();
        for (int it = 0; it < 100; it++) {
            BigInteger b1 = new BigInteger(50, rand); // 2**50
            BigInteger b2 = new BigInteger(50, rand); // 2**50
            HugeInteger h1 = new HugeInteger(b1.toString());
            HugeInteger h2 = new HugeInteger(b2.toString());


            String[] keys = {
                "h1",
                "h2",
                "h1 == 0",
                "h1 == h2",
                "h1 != h2",
                "h1 < h2",
                "h1 <= h2",
                "h1 > h2",
                "h1 >= h2",
                "h1 + h2",
                "h2 - h2",
                "h1 * h2",
                "h1 / h2",
                "h1 % h2"
            };

            String[] ans = {
                String.format("%s", b1),
                String.format("%s", b2),
                String.format("%b", b1.equals(0)),
                String.format("%b", b1.equals(b2)),
                String.format("%b", !b1.equals(b2)),
                String.format("%b", b1.compareTo(b2) == -1),
                String.format("%b", b1.compareTo(b2) <= 0),
                String.format("%b", b1.compareTo(b2) == +1),
                String.format("%b", b1.compareTo(b2) >= 0),
                String.format("%s", b1.add(b2)),
                String.format("%s", b1.subtract(b2)),
                String.format("%s", b1.multiply(b2)),
                String.format("%s", b1.divide(b2)),
                String.format("%s", b1.mod(b2))
            };

            String[] res = {
                String.format("%s", h1),
                String.format("%s", h2),
                String.format("%b", h1.isZero()),
                String.format("%b", h1.isEqualTo(h2)),
                String.format("%b", h1.isNotEqualTo(h2)),
                String.format("%b", h1.isLessThan(h2)),
                String.format("%b", h1.isLessThanOrEqual(h2)),
                String.format("%b", h1.isGreaterThan(h2)),
                String.format("%b", h1.isGreaterThanOrEqual(h2)),
                String.format("%s", h1.add(h2)),
                String.format("%s", h1.subtract(h2)),
                String.format("%s", h1.multiply(h2)),
                String.format("%s", h1.divide(h2)),
                String.format("%s", h1.remainder(h2))
            };

            for (int i = 0; i < keys.length; i++) {
                if (!ans[i].equals(res[i])) {
                    System.out.printf("%s, %s\n", b1, b2);
                    System.out.printf("Error of %s\n", keys[i]);
                    System.out.printf("Expected: %s\n", ans[i]);
                    System.out.printf("Result  : %s\n", res[i]);
                    System.out.println();
                }
            }
        }
    }
}
