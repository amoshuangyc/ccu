public class RationalTest extends Object {

    private void test(String exp, String res, Rational r) {
        System.out.printf("Test %s:\t(%s) %s\n", exp, res, r);
    }

    private void test_basic() {
        test("Rational()", "0/1", new Rational());
        test("Rational(63, 21)", "3/1", new Rational(63, 21));
        test("Rational(-0, 100)", "0/1", new Rational(-0, 100));
        test("Rational(4, -6)", "-2/3", new Rational(4, -6));
    }

    private void test_arith() {
        Rational r1 = new Rational(3, 6);
        Rational r2 = new Rational(3, 4);
        System.out.printf("r1 = %s, r2 = %s\n", r1, r2);
        test("r1 + r2", "5/4", Rational.add(r1, r2));
        test("r1 - r2", "-1/4", Rational.sub(r1, r2));
        test("r1 * r2", "3/8", Rational.mul(r1, r2));
        test("r1 / r2", "3/2", Rational.div(r1, r2));
    }

    private void test_print() {
        System.out.println("Test -3/9:");
        System.out.printf("%%s:\t%s\n", new Rational(-3, 9));
        System.out.printf(".5f:\t%s\n", new Rational(-3, 9).toFloatString(5));
        System.out.printf(".10f:\t%s\n", new Rational(-3, 9).toFloatString(10));

        System.out.println();

        System.out.println("Test 1/7:");
        System.out.printf("%%s:\t%s\n", new Rational(1, 7));
        System.out.printf(".5f:\t%s\n", new Rational(1, 7).toFloatString(5));
        System.out.printf(".10f:\t%s\n", new Rational(1, 7).toFloatString(10));
    }

    private void main() {
        System.out.println();
        test_basic();
        System.out.println();
        test_arith();
        System.out.println();
        test_print();
        System.out.println();
    }

    public static void main(String[] args) {
        new RationalTest().main();
    }
}
