public class Rational extends Object {
    private int num, den;

    public Rational() {
        this.num = 0;
        this.den = 1;
    }

    public Rational(int n, int d) {
        this.num = n;
        this.den = d;
        this.simplify();
    }

    private void simplify() {
        if (this.den == 0) {
            throw new IllegalArgumentException("denumerator cannot be 0");
        }

        if (this.den < 0) {
            this.den = -this.den;
            this.num = -this.num;
        }

        int g = this.gcd(Math.abs(this.num), Math.abs(this.den));
        this.num /= g;
        this.den /= g;
    }

    private static int gcd(int a, int b) {
        while (b != 0) {
            int temp = a % b;
            a = b;
            b = temp;
        }
        return a;
    }

    public boolean isZero() {
        return this.num == 0;
    }

    public static Rational add(Rational r1, Rational r2) {
        int n = r1.num * r2.den + r2.num * r1.den;
        int d = r1.den * r2.den;
        return new Rational(n, d);
    }

    public static Rational sub(Rational r1, Rational r2) {
        int n = r1.num * r2.den - r2.num * r1.den;
        int d = r1.den * r2.den;
        return new Rational(n, d);
    }

    public static Rational mul(Rational r1, Rational r2) {
        int n = r1.num * r2.num;
        int d = r1.den * r2.den;
        return new Rational(n, d);
    }

    public static Rational div(Rational r1, Rational r2) {
        if (r2.isZero()) {
            throw new IllegalArgumentException("Divided by 0");
        }

        int n = r1.num * r2.den;
        int d = r1.den * r2.num;
        return new Rational(n, d);
    }

    public String toString() {
        return String.format("%d/%d", this.num, this.den);
    }

    public String toFloatString(int precision) {
        String fmt = String.format("%%.%df", precision);
        double val = this.num * 1.0 / this.den;
        return String.format(fmt, val);
    }
}
