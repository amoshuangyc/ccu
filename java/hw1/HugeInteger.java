import java.util.Arrays;

public class HugeInteger extends Object {
    private int sign;
    private int len;
    private int[] data;

    public HugeInteger(int sign, int len, int[] data) {
        this.sign = sign;
        this.len = len;
        this.data = Arrays.copyOf(data, data.length);

        // remove leading zero
        while (this.len > 1 && this.data[this.len - 1] == 0) {
            this.len--;
        }
    }

    public HugeInteger(int val) {
        this.len = 1;
        this.data = new int[40];

        if (val == 0) {
            this.sign = 0;
            return;
        }
        this.sign = ((val < 0) ? -1 : +1);

        val = Math.abs(val);
        while (val != 0) {
            this.data[this.len - 1] = val % 10;
            this.len++;
            val /= 10;
        }
    }

    public HugeInteger(String s) {
        // follow regex: "([+|-]?)(\\d+)"

        if (s.equals("0") || s.equals("-0") || s.equals("+0")) {
            this.sign = 0;
            this.len = 1;
            this.data = new int[40];
            return;
        }

        String digits;

        // sign
        if (s.charAt(0) == '+') {
            this.sign = +1;
            digits = s.substring(1);
        }
        else if (s.charAt(0) == '-') {
            this.sign = -1;
            digits = s.substring(1);
        }
        else {
            this.sign = +1;
            digits = s;
        }

        // digits
        data = new int[40];
        this.len = digits.length();
        for (int i = 0; i < this.len; i++) {
            this.data[len - i - 1] = digits.charAt(i) - '0' + 0;
        }

        // remove leading zero
        while (this.len > 1 && this.data[this.len - 1] == 0) {
            this.len--;
        }
    }

    public boolean isEqualTo(HugeInteger h) {
        if (this.sign != h.sign) {
            return false;
        }
        return Arrays.equals(this.data, h.data);
    }

    public boolean isNotEqualTo(HugeInteger h) {
        return !this.isEqualTo(h);
    }

    public boolean isLessThan(HugeInteger h) {
        // -a < +b
        if (this.sign == -1 && h.sign == +1) {
            return true;
        }
        // -a < -b => a > b
        if (this.sign == -1 && h.sign == -1) {
            return this.abs().isGreaterThan(h.abs());
        }
        // +a < -b
        if (this.sign == +1 && h.sign == -1) {
            return false;
        }

        if (this.len < h.len) {
            return true;
        }
        if (this.len > h.len) {
            return false;
        }

        for (int i = this.len - 1; i >= 0; i--) {
            if (this.data[i] < h.data[i]) {
                return true;
            }
            if (this.data[i] > h.data[i]) {
                return false;
            }
        }
        return false;
    }

    public boolean isLessThanOrEqual(HugeInteger h) {
        return this.isLessThan(h) || this.isEqualTo(h);
    }

    public boolean isGreaterThan(HugeInteger h) {
        return !this.isLessThan(h) && this.isNotEqualTo(h);
    }

    public boolean isGreaterThanOrEqual(HugeInteger h) {
        return !this.isLessThan(h);
    }

    public boolean isZero() {
        return this.sign == 0;
    }

    public HugeInteger abs() {
        return new HugeInteger(Math.abs(this.sign), this.len, this.data);
    }

    public HugeInteger neg() {
        return new HugeInteger(-this.sign, this.len, this.data);
    }

    public HugeInteger add(HugeInteger h) {
        if (this.sign == 0) {
            return new HugeInteger(h.sign, h.len, h.data);
        }
        if (h.sign == 0) {
            return new HugeInteger(this.sign, this.len, this.data);
        }
        HugeInteger abs_a = this.abs();
        HugeInteger abs_b = h.abs();
        // (-a) + (+b) = b - a
        if (this.sign == -1 && h.sign == +1) {
            return abs_b.subtract(abs_a);
        }
        // (-a) + (-b) = -(a + b)
        if (this.sign == -1 && h.sign == -1) {
            return abs_a.add(abs_b).neg();
        }
        // (+a) + (-b) = a - b
        if (this.sign == +1 && h.sign == -1) {
            return abs_a.subtract(abs_b);
        }

        int len = Math.max(this.len, h.len);
        int[] res = new int[40];

        int carry = 0;
        for (int i = 0; i < len; i++) {
            int sum = this.data[i] + h.data[i] + carry;
            res[i] = sum % 10;
            carry = sum / 10;
        }
        while (carry != 0) {
            res[len++] = carry % 10;
            carry /= 10;
        }

        return new HugeInteger(+1, len, res);
    }

    public HugeInteger subtract(HugeInteger h) {
        if (this.sign == 0) {
            return new HugeInteger(-h.sign, h.len, h.data);
        }
        if (h.sign == 0) {
            return new HugeInteger(this.sign, this.len, this.data);
        }
        HugeInteger abs_a = this.abs();
        HugeInteger abs_b = h.abs();
        // (-a) - (+b) = -(a + b)
        if (this.sign == -1 && h.sign == +1) {
            return abs_a.add(abs_b).neg();
        }
        // (-a) - (-b) = b - a
        if (this.sign == -1 && h.sign == -1) {
            return abs_b.subtract(abs_a);
        }
        // (+a) - (-b) = a + b
        if (this.sign == +1 && h.sign == -1) {
            return abs_a.add(abs_b);
        }
        // (+a) - (+b) where a < b
        if (abs_a.isLessThan(abs_b)) {
            return abs_b.subtract(abs_a).neg();
        }

        int len = Math.max(this.len, h.len);
        int[] res = new int[40];

        int borrow = 0;
        for (int i = 0; i < len; i++) {
            int diff = this.data[i] - h.data[i] - borrow;
            borrow = 0;
            while (diff < 0) {
                borrow++;
                diff += 10;
            }
            res[i] = diff;
        }

        return new HugeInteger(+1, len, res);
    }

    public HugeInteger multiply(HugeInteger h) {
        int sign = this.sign * h.sign;
        int len = this.len + h.len;
        int[] res = new int[40];

        for (int i = 0; i < this.len; i++) {
            for (int j = 0; j < h.len; j++) {
                res[i + j] += this.data[i] * h.data[j];
            }
        }
        int carry = 0;
        for (int i = 0; i < len; i++) {
            int val = res[i] + carry;
            res[i] = val % 10;
            carry = val / 10;
        }
        while (carry != 0) {
            res[len++] = carry % 10;
            carry /= 10;
        }

        return new HugeInteger(sign, len, res);
    }

    public HugeInteger[] divremainder(HugeInteger h) {
        if (h.isZero()) {
            throw new IllegalArgumentException("Divided by 0");
        }

        if (this.sign == -1 && h.sign == -1) {
            return this.abs().divremainder(h.abs());
        }
        if (this.sign == -1 && h.sign == +1) {
            HugeInteger[] res = this.abs().divremainder(h);
            res[0] = res[0].neg();
            if (res[0].sign == -1) {
                res[0] = res[0].subtract(new HugeInteger(1));
                res[1] = res[1].neg().add(h);
            }
            return res;
        }
        if (this.sign == +1 && h.sign == -1) {
            return this.neg().divremainder(h.neg());
        }

        HugeInteger[] divisors = new HugeInteger[10];
        divisors[0] = new HugeInteger(0);
        for (int t = 1; t < 10; t++) {
            divisors[t] = divisors[t - 1].add(h);
        }

        int q_sign = this.sign * h.sign;
        int q_len = this.len;
        int[] q_data = new int[40];

        HugeInteger val = new HugeInteger(0);
        for (int i = this.len - 1; i >= 0; i--) {
            HugeInteger digit = new HugeInteger(this.data[i]);
            val = val.multiply(new HugeInteger(10)).add(digit);

            if (val.isLessThan(h)) {
                continue;
            }

            for (int t = 9; t >= 1; t--) {
                if (divisors[t].isLessThanOrEqual(val)) {
                    q_data[i] = t;
                    val = val.subtract(divisors[t]);
                    break;
                }
            }
        }

        HugeInteger q = new HugeInteger(q_sign, q_len, q_data);
        HugeInteger[] res = new HugeInteger[2];
        res[0] = q;
        res[1] = val;
        return res;
    }

    public HugeInteger divide(HugeInteger h) {
        HugeInteger[] res = this.divremainder(h);
        return res[0];
    }

    public HugeInteger remainder(HugeInteger h) {
        HugeInteger[] res = this.divremainder(h);
        return res[1];
    }

    public String toString() {
        StringBuffer res = new StringBuffer();
        if (this.sign == -1) {
            res.append('-');
        }
        for (int i = this.len - 1; i >= 0; i--) {
            res.append(this.data[i]);
        }
        return res.toString();
    }

    public static void main(String[] args) {
        // Test Constructor
        System.out.println("Constructor");
        System.out.printf("HugeInteger(-12): %s\n", new HugeInteger("-12"));
        System.out.printf("HugeInteger(+56): %s\n", new HugeInteger("+56"));
        System.out.printf("HugeInteger(-0): %s\n", new HugeInteger("-0"));
        System.out.printf("HugeInteger(987654321987654321): %s\n", new HugeInteger("987654321987654321"));
        System.out.println("--------------");

        // Test Comparison
        System.out.println("Comparison");
        HugeInteger h1 = new HugeInteger("-52468971236987");
        HugeInteger h2 = new HugeInteger("-62468971236987");
        System.out.printf("h1 = %s\nh2 = %s\n", h1, h2);
        System.out.printf("h1 == 0 : %b\n", h1.isZero());
        System.out.printf("h1 == h2 : %b\n", h1.isEqualTo(h2));
        System.out.printf("h1 != h2 : %b\n", h1.isNotEqualTo(h2));
        System.out.printf("h1 < h2 : %b\n", h1.isLessThan(h2));
        System.out.printf("h1 > h2 : %b\n", h1.isGreaterThan(h2));
        System.out.printf("h1 >= h2 : %b\n", h1.isGreaterThanOrEqual(h2));
        System.out.println("--------------");

        // Test Add/Sub
        System.out.println("Add/Sub");
        h1 = new HugeInteger("-52468971236987");
        // h1 = new HugeInteger("52468971236987");
        // h2 = new HugeInteger("-62468971236987");
        h2 = new HugeInteger("100");
        System.out.printf("h1 = %s\nh2 = %s\n", h1, h2);
        System.out.printf("h1 + h2 : %s\n", h1.add(h2));
        System.out.printf("h1 - h2 : %s\n", h1.subtract(h2));
        System.out.printf("h1 * h2 : %s\n", h1.multiply(h2));
        System.out.printf("h1 / h2 : %s\n", h1.divide(h2));
        System.out.printf("h1 %% h2 : %s\n", h1.remainder(h2));
        System.out.println("--------------");

        System.out.printf("%b\n", new HugeInteger("-3000").isLessThan(new HugeInteger("-10")));
        System.out.printf("%s\n", new HugeInteger("100").subtract(new HugeInteger("1")));
    }
}
