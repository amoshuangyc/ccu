# HW 1

## 8.15 Rational

### Compile & Exec

```
javac Rational.java
javac -d ./ RationalTest.java
javac RationalTest
```

可以正確處理負數，且如果出現「分母為 0」或「除以 0」，程式會丟出 `IllegalArgumentException`。

## 8.16 HugeInteger

### Compile & Exec

```
javac HugeInteger.java && java HugeInteger
```

我另外有寫一個測試程式:

```
javac -d ./ HugeIntegerTest.java && java HugeIntegerTest
```

可以正確處理負數，建構子合法的字串是符合 re `([+|-]?)(\\d+)`。沒有防呆，運算超出 40 位直接 Runtime Error。另外，除以 0 會有 `IllegalArgumentException`。

負數的除法與取餘數是模彷 python 的運算，即

```
-5 / 3 = -2 ... 1
```

這種設計確保了 `a / b` 時，`a = b * (a / b) + (a % b)` 恆成立。
