# HW 2

403410034 資工三 黃鈺程

## 第一題

在 `p1` 資料夾下：

```
javac -d classes -sourcepath src src/shapes/*.java
javac -d classes -sourcepath src src/ShapeTest.java
java -cp classes ShapeTest
```

有防呆，當給定的值不合法時，程式會丟出 `IllegalArgumentException`。

## 第二題

在 `p2` 資料夾下：

```
javac -d ./ *.java
java PayrollSystemTest
```

可以看到 `PayrollSystemTest.java` 中都改成 `getPaymentAmount()` 了
