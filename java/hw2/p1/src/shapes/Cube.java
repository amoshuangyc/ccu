package shapes;

public class Cube extends ThreeDimensionalShape {
    private double l;
    public Cube(double l) {
        if (l <= 0) {
            throw new IllegalArgumentException("Not forming a Cube");
        }
        this.l = l;
    }

    @Override
    public double getArea() {
        return this.l * this.l * 6.0;
    }

    @Override
    public double getVolume() {
        return this.l * this.l * this.l;
    }
}
