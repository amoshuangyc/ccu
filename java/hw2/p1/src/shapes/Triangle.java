package shapes;

public class Triangle extends TwoDimensionalShape {
    private double l1, l2, l3;

    public Triangle(double l1, double l2, double l3) {
        if (l1 <= 0 || l2 <= 0 || l3 <= 0 ||
            l1 >= l2 + l3 || l2 >= l1 + l3 || l3 >= l1 + l2) {
            throw new IllegalArgumentException("Not forming a Triangle");
        }
        this.l1 = l1;
        this.l2 = l2;
        this.l3 = l3;
    }

    public double getArea() {
        double s = (this.l1 + this.l2 + this.l3) / 2.0;
        return Math.sqrt(s * (s - this.l1) * (s - this.l2) * (s - this.l3));
    }
}
