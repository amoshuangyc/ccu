package shapes;

public class Square extends TwoDimensionalShape {
    private double l;

    public Square(double l) {
        if (l <= 0.0) {
            throw new IllegalArgumentException("Not forming a Square");
        }
        this.l = l;
    }

    @Override
    public double getArea() {
        return this.l * this.l;
    }
}
