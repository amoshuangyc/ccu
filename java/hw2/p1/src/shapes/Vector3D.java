package shapes;

public class Vector3D {
    public double x, y, z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double norm() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public static Vector3D add(Vector3D a, Vector3D b) {
        return new Vector3D(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Vector3D sub(Vector3D a, Vector3D b) {
        return new Vector3D(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static Vector3D cross(Vector3D a, Vector3D b) {
        // |  i  j  k |
        // | x1 y1 z1 |
        // | x2 y2 z2 |
        // = (y1z2 - y2z1)i + (z1x2 - z2x1)j + (x1y2 - x2y1)k
        double x = a.y*b.z - b.y*a.z;
        double y = a.z*b.x - b.z*a.x;
        double z = a.x*b.y - b.x*a.y;
        return new Vector3D(x, y, z);
    }

    public static double dot(Vector3D a, Vector3D b) {
        return a.x*b.x + a.y*b.y + a.z*b.z;
    }

    public String toString() {
        return String.format("(%f, %f, %f)", this.x, this.y, this.z);
    }
}
