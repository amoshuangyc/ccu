package shapes;

public class Sphere extends ThreeDimensionalShape {
    private double r;

    public Sphere(double r) {
        if (r <= 0) {
            throw new IllegalArgumentException("Not forming a Sphere");
        }
        this.r = r;
    }

    @Override
    public double getArea() {
        return 4.0 * Math.PI * this.r * this.r;
    }

    @Override
    public double getVolume() {
        return 4.0 / 3.0 * Math.PI * this.r * this.r * this.r;
    }
}
