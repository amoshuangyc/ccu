package shapes;

public class Circle extends TwoDimensionalShape {
    private double r;

    public Circle(double r) {
        if (r <= 0.0) {
            throw new IllegalArgumentException("Not forming a Circle");
        }
        this.r = r;
    }

    @Override
    public double getArea() {
        return Math.PI * this.r * this.r;
    }
}
