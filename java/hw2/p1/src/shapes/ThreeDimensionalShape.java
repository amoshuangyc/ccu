package shapes;

public abstract class ThreeDimensionalShape extends Shape {
    public double getArea() {
        return 0.0;
    }
    public double getVolume() {
        return 0.0;
    }
    public String toString() {
        return "3D Shape";
    }
}
