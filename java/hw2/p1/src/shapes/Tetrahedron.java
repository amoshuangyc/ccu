package shapes;

public class Tetrahedron extends ThreeDimensionalShape {
    private Vector3D p1, p2, p3, p4;

    public Tetrahedron(Vector3D p1, Vector3D p2, Vector3D p3, Vector3D p4) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        if (Math.abs(this.getVolume()) < 1e-10) { // volume = 0
            throw new IllegalArgumentException("Not forming a Tetrahedron");
        }
    }

    @Override
    public double getArea() {
        Vector3D v12 = Vector3D.sub(this.p2, this.p1);
        Vector3D v13 = Vector3D.sub(this.p3, this.p1);
        Vector3D v14 = Vector3D.sub(this.p4, this.p1);
        Vector3D v23 = Vector3D.sub(this.p3, this.p2);
        Vector3D v24 = Vector3D.sub(this.p4, this.p2);
        double area1 = Vector3D.cross(v12, v13).norm() / 2.0; // traingle 123
        double area2 = Vector3D.cross(v12, v14).norm() / 2.0; // triangle 124
        double area3 = Vector3D.cross(v13, v14).norm() / 2.0; // triangle 134
        double area4 = Vector3D.cross(v23, v24).norm() / 2.0; // triangle 234
        return area1 + area2 + area3 + area4;
    }

    @Override
    public double getVolume() {
        Vector3D a = Vector3D.sub(this.p2, this.p1);
        Vector3D b = Vector3D.sub(this.p3, this.p1);
        Vector3D c = Vector3D.sub(this.p4, this.p1);
        return Vector3D.dot(a, Vector3D.cross(b, c)) / 6.0;
    }
}
