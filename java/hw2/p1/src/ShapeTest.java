import java.util.Scanner;
import shapes.*;

public class ShapeTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter num: ");
        double num = input.nextDouble();

        Shape[] shapes = {
            new Circle(num),
            new Square(num),
            new Triangle(num, num, num),
            new Sphere(num),
            new Cube(num),
            new Tetrahedron(
                new Vector3D(0.0, 0.0, 0.0), new Vector3D(num, 0.0, 0.0),
                new Vector3D(0.0, num, 0.0), new Vector3D(0.0, 0.0, num)
            )
        };

        for (Shape x : shapes) {
            if (x instanceof TwoDimensionalShape) {
                TwoDimensionalShape xx = (TwoDimensionalShape) x;
                System.out.printf("2D area = %f\n", xx.getArea());
            }
            else {
                ThreeDimensionalShape xx = (ThreeDimensionalShape) x;
                System.out.printf("3D area = %f\n", xx.getArea());
                System.out.printf("3D volume = %f\n", xx.getVolume());
            }
        }
    }
}
