import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Hw3 extends JFrame implements ActionListener, KeyListener {
    String target;
    JTextArea txt;
    HashMap<String, JButton> keyboard;

    public Hw3() {
        super("Hw3");
        this.setSize(805, 550);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.initUI();
        this.setVisible(true);
    }

    public void initUI() {
        Container pane = getContentPane();
        pane.setLayout(null);

        this.target = new String("The quick brown fox jumps over the lazy dog");
        JLabel label = new JLabel(target);
        label.setBounds(5, 0, 600, 50);
        label.setBackground(Color.green);
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        pane.add(label);

        JButton stat = new JButton("Statistics");
        stat.setBounds(700, 5, 100, 40);
        stat.setMargin(new Insets(0, 0, 0, 0));
        stat.addActionListener(this);
        pane.add(stat);

        this.txt = new JTextArea("");
        this.txt.setBounds(5, 55, 795, 195);
        this.txt.setBackground(Color.WHITE);
        this.txt.setFont(new Font("Arial", Font.PLAIN, 20));
        this.txt.setMargin(new Insets(10, 10, 10, 10));
        this.txt.setLineWrap(true);
        this.txt.addKeyListener(this);
        pane.add(this.txt);

        this.keyboard = new HashMap<String, JButton>();
        String[][] keys = {
            {"`", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "=", "Backspace"},
            {"Tab", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "\\"},
            {"Caps", "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "Enter"},
            {"Shift", "Z", "X", "C", "V", "B", "N", "M", ",", ".", "?"},
            {"Space", "↑", "↓", "←", "→"}
        };
        // row 0
        for (int i = 0; i < keys[0].length; i++) {
            String key = keys[0][i];
            int x = 50 * i;
            int y = 250;
            int width = ((key.equals("Backspace")) ? 150 : 50);
            pane.add(createKeyButton(key, x, y, width));
        }
        // row 1
        for (int i = 0; i < keys[1].length; i++) {
            String key = keys[1][i];
            int x = ((i >= 1) ? 75 + (i - 1) * 50 : 0);
            int y = 300;
            int width = ((key.equals("Tab")) ? 75 : 50);
            pane.add(createKeyButton(key, x, y, width));
        }
        // row 2
        for (int i = 0; i < keys[2].length; i++) {
            String key = keys[2][i];
            int x = ((i >= 1) ? 75 + (i - 1) * 50 : 0);
            int y = 350;
            int width = 50;
            if (key.equals("Caps")) width = 75;
            if (key.equals("Enter")) width = 100;
            pane.add(createKeyButton(key, x, y, width));
        }
        // row 3
        for (int i = 0; i < keys[3].length; i++) {
            String key = keys[3][i];
            int x = ((i >= 1) ? 100 + (i - 1) * 50 : 0);
            int y = 400;
            int width = 50;
            if (key.equals("Shift")) width = 100;
            pane.add(createKeyButton(key, x, y, width));
        }
        // other
        for (int i = 0; i < keys[4].length; i++) {
            String key = keys[4][i];
            int x = 0, y = 0, width = 50;
            if (key.equals("Space")) {
                x = 200;
                y = 450;
                width = 250;
            }
            if (key.equals("↑")) {
                x = 625;
                y = 400;
            }
            if (key.equals("←")) {
                x = 575;
                y = 450;
            }
            if (key.equals("↓")) {
                x = 625;
                y = 450;
            }
            if (key.equals("→")) {
                x = 675;
                y = 450;
            }
            pane.add(createKeyButton(key, x, y, width));
        }
    }

    public void actionPerformed(ActionEvent e) {
        String type = this.txt.getText();
        int cnt = 0;
        for (int i = 0; i < Math.min(type.length(), target.length()); i++) {
            if (type.charAt(i) == target.charAt(i)) {
                cnt++;
            }
        }
        String res = String.format("%d/%d", cnt, target.length());
        JOptionPane.showMessageDialog(this, res, "Accuracy", JOptionPane.PLAIN_MESSAGE);
    }

    private JButton createKeyButton(String s, int x, int y, int w) {
        JButton btn = new JButton(s);
        btn.setMargin(new Insets(0, 0, 0, 0)); // padding = 0 to get rid of ...
        btn.setBounds(x + 5, y + 5, w - 5, 50 - 5);
        this.keyboard.put(s, btn);
        return btn;
    }

    private JButton getKeyButton(KeyEvent e) {
        String s = KeyEvent.getKeyText(e.getKeyCode());
        if (s.equals("Back Quote")) s = "`";
        if (s.equals("Minus")) s = "-";
        if (s.equals("Equals")) s = "=";
        if (s.equals("Open Bracket")) s = "[";
        if (s.equals("Close Bracket")) s = "]";
        if (s.equals("Back Slash")) s = "\\";
        if (s.equals("Caps Lock")) s = "Caps";
        if (s.equals("Semicolon")) s = ";";
        if (s.equals("Quote")) s = "'";
        if (s.equals("Comma")) s = ",";
        if (s.equals("Period")) s = ".";
        if (s.equals("Slash")) s = "?";
        if (s.equals("Up")) s = "↑";
        if (s.equals("Left")) s = "←";
        if (s.equals("Down")) s = "↓";
        if (s.equals("Right")) s = "→";
        return this.keyboard.get(s);
    }

    public void keyPressed(KeyEvent e) {
        JButton btn = getKeyButton(e);
        if (btn != null) {
            btn.setBackground(Color.orange);
        }
    }

    public void keyReleased(KeyEvent e) {
        JButton btn = getKeyButton(e);
        if (btn != null) {
            btn.setBackground(new JButton().getBackground());
        }
    }

    public void keyTyped(KeyEvent e) {
        ;
    }

    public static void main(String[] args) {
        new Hw3();
    }
}
