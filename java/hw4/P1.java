import java.util.Scanner;
import java.util.HashMap;

public class P1 {
    public P1() {
        Scanner input = new Scanner(System.in);
        System.out.print("Input sentence: ");
        String inp = input.nextLine();

        inp = inp.toLowerCase();
        inp = inp.replaceAll("[^a-zA-Z0-9 ]", "");
        String[] words = inp.split(" ");

        HashMap<String, Integer> freq = new HashMap<String, Integer>();
        for (String w : words) {
            // System.out.printf("|%s|\n", w);
            if (w.isEmpty()) continue;

            if (freq.containsKey(w)) {
                int v = freq.get(w);
                freq.put(w, v + 1);
            }
            else {
                freq.put(w, 1);
            }
        }

        System.out.printf("word      | freq(>1)\n");
        System.out.println("----------------------------");
        int cnt = 0;
        for (String k : freq.keySet()) {
            int v = freq.get(k);
            if (v > 1) {
                cnt++;
                System.out.printf("%-10s|%9d\n", k, v);
            }
        }
        System.out.println("----------------------------");
        System.out.printf("%d duplicate word(s) in total\n", cnt);
    }

    public static void main(String[] args) {
        new P1();
    }
}
