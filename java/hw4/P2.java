import java.util.Scanner;
import java.util.TreeSet;
import java.util.Iterator;

public class P2 {
    public P2() {
        Scanner input = new Scanner(System.in);
        System.out.print("Input sentence: ");
        String inp = input.nextLine();

        inp = inp.toLowerCase();
        inp = inp.replaceAll("[^a-zA-Z0-9 ]", "");
        String[] words = inp.split(" ");

        TreeSet<String> set = new TreeSet<String>();
        for (String w : words) {
            if (w.isEmpty()) continue;
            set.add(w);
        }

        Iterator it = set.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }

    public static void main(String[] args) {
        new P2();
    }
}
