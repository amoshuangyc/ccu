# JAVA HW4

403410034 資工三 黃鈺程

編譯指令為

```
javac -d ./ ./P2.java
java P2
```

## 第二題

### 功能

讀入一整行字串，依字典順序輸出該行中的所有 words（如同前一題，標點符號是忽略的）

## 流程

1. 讀入整行字串
2. 將字串中「非英文字母、非數字、非空白」的字元移除，使用 regex
3. 使用 split 將字串切成 words
4. 使用 TreeSet 排序 words
5. 輸出

### 範例

```
Input sentence: aa e'a ea ae gg gg' 'gg
aa
ae
ea
gg
```

```
Input sentence: here i am i am here friend
am
friend
here
i
```
