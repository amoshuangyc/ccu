# JAVA HW4

403410034 資工三 黃鈺程

編譯指令為

```
javac -d ./ ./P1.java
java P1
```

## 第一題

### 功能

讀入一整行字串，輸出字串中所有的 duplicate words 與他們的頻率。

### 流程

1. 讀入整行字串
2. 將字串中「非英文字母、非數字、非空白」的字元移除，使用 regex
3. 使用 split 將字串切成 words
4. 使用 HashMap 計算各 word 的頻率（出現次數）
5. 輸出

### 範例

```
Input sentence: aa e'a ea ae gg gg' 'gg
word      | freq(>1)
----------------------------
gg        |        3
ea        |        2
----------------------------
2 duplicate word(s) in total
```

```
Input sentence: here i am i am here friend
word      | freq(>1)
----------------------------
here      |        2
i         |        2
am        |        2
----------------------------
3 duplicate word(s) in total
```
