# 作業二

403410034 資工四 黃鈺程

## Environment

※ 不在工作站跑因為工作站沒辦法調 ulimit，只有 256 根本什麼都跑不了~

- Docker
  - Fedora 26
  - 1GB Memory
  - 1GB Swap
  - i5
- **HDD**

## Dataset

使用同學爬蟲爬下來的 PTT 版： C_Chat ，該版共計 997 MB, 共 21,194,483 行。
我使用以下指令將檔案不同大小 100MB, 200MB, ..., 900MB，以方便之後測試。

```sh
for (( i=100; i<1000; i+=100 )); \
  do head -c "${i}m" C_Chat_data.txt > $i && echo $i; done
```

## Algorithms

我使用 Go 實作了 2 種外部排序 `merge sort` 與 `partition sort`：

1. Go: Ngrams + Ext. MergeSort + Word Count + Ext. MergeSort
2. Go: Ngrams + Ext. PartitionSort + Word Count + Ext. PartitionSort

之後簡稱 `Merge Sort`, `Parti Sort`。

我所有的程式碼可以在我的 [gitlab](https://gitlab.com/amoshuangyc/ccu/tree/master/data-engineering/hw2) 上找到。

## File Structure

```
├── benchmark.py
├── hw2go
│   ├── external_merge_sort.go
│   ├── external_partition_sort.go
│   ├── hw2go
│   ├── main.go
│   ├── ngram.go
│   ├── util.go
│   └── word_count.go
├── result.csv
└── test.py
```

`test.py` 是用來測演算法的正確性，將 Go 跑出來的結果與驗證程式比對。驗證程式是由 Python 撰寫，使用 In Memory Hash + Sort。`benchmark.py` 是用來測參數、演算法對執行時間的影響，`result.csv` 是它產出來的結果。

`hw2go/` 中放的是 Go 的程式碼，程式碼經過許多次重構，基本符合 Go 的 Coding 習慣。程式執行中的暫存檔案（chunks）都放到了 `/tmp/hw2/` 中，並會在程式結束時刪除，最後程式只輸出 `out.{s}.{n}`，`s` 代表資料量，`n` 代表 ngrams 的 n。例如 500MB 時會產出 `out.500.4`, `out.500.5`, ..., `out.500.9` 分別代表 4grams, 5grams, ..., 9grams 統計排序後的結果。

編譯 Go 為在 `hw2go/` 中執行

```sh
go build
```

程式的參數為：

```sh
[amoshyc@f26 hw2]$ ./hw2go/hw2go -h
Usage of ./hw2go/hw2go:
  -a string
    	algorithm (default "merge")
  -i string
    	input path (default "/dev/stdin")
  -o string
    	output path (default "/dev/stdout")
  -p int
    	parameter (default 1000)
```


## Difficulties

開發途中遇到許多困難，以下一一列出。

### Go package

我搞不太懂 Go 的 package 是如何架構的，官方推薦的方法也不太適合這個作業。
即產出一個 binary 並被其他程式（`benchmark.py`, `test.py`）調動，同時程式碼又有被 git 管理。
Go 的設計似乎強迫你將程式碼架構成 Library 的型式…
總之我最後的解決方法為將所有程式碼放在 `main` package 中。

### MergeSort Buffer

我使用 Go 標準函式庫中的 bufio 作為我檔案輸出輸出的操作。在撰寫 `Merge Sort` 時我原先在 bufio 上又架了一層 buffer，因為我想 bufio 是使用 bytes 來決定 buffer 大小，所以我加了一層基於 line 的 buffer 來抽象化。但這樣寫出來的程式會正確，執行時間卻非常慢，慢到跟 Unix 內建的 `sort` 相同時間。一旦將這層抽象化拔掉，`Merge Sort` 的速度就變得極快~我目前還沒想通為什麼會這樣。

### MergeSort Winner Tree

這個作業中的 Winner Tree 並不是很好寫，因為同時要跟 Buffer 互動，我原先太想寫出一個通用版本的 Winner Tree，但最後發現真的太難寫了，才轉用競賽寫法來寫 Winner Tree，即使用全域變數。最後一次重構才又將 Winner Tree 包進 Struct 中。

### Limit of Opened Files

在 Linux 中，每個 process 同時能開啟的檔案數量是有限制的，我的系統預設是 1024，通常在開啟第 1019 個檔案時，程式就會 RE(panic)。這個上限可以透過 `ulimit -n` 來查到，也可以透過相同指令更改，不過似乎要給予 root 才能將這個上限調大。

## Parameters

因為使用 python 或 Unix 內建的 sort 來執行都過於緩慢，所以我並沒有將他們列入 benchmark。
所以比較的程式只有 `Merge Sort`, `Parti Sort`。另外檔案雖然有 997MB，但我 benchmark 跑了一整晚還跑不完，所以實際結果中只有目前跑出來的 100MB, 300MB, 500MB。

`Merge Sort` 有一個參數 `chunkSize` 代表每個 chunk 是多少行。`Parti Sort` 有一個參數 `pivotItv` 代表隔多少行取一次 pivot，也就是說，中間會有 `pivotItv + 1` 個 chunk 產生。

### 不同 DataSize

![](https://i.imgur.com/meL4bym.png)

圖片的 error 代表不同 chunkSize/pivotItv 時跑出來的不同結果，取中位數作為代表。

從圖中可以看到，`Merge Sort` 的表現比 `Parti Sort` 還好，但沒有好上多少。這個結果與我預期的不一樣，因為我在測 10MB 等級的資料時，`Parti Sort` 是跑出比較好的結果的，想不到資料量一大，結果就反過來了。

4grams ~ 9grams 共跑了約 2500s，即 40 分鐘左右，平均下來，每個 ngram 跑了 8 分鐘多，感覺有點慢，但也沒到不能接受的地步。聽說是 Go 的 io 比 C 的慢上許多造成的。

### 不同 ChunkSize/PivotItv

![](https://i.imgur.com/efWZ03T.png)

![](https://i.imgur.com/TVcMNJG.png)

理論上

```
當 chunkSize 越大時，chunk 的數量是越來越少，所以執行時間越來越少。
當 pivotItv 越來越大時，chunk 的數量反而是越來越多的，所以執行時間越來越多。
```

但圖表中沒表現出這個趨勢…。從圖表中的結果來看，chunkSize/pivotItv 對執行時間的影響不大，這讓人不解。對這種情況，我目前沒有很好的解釋，也許是我的 code 壞掉了吧~要不然就是我想得太簡單了，影響執行時間的參數太多，可能彼此抵消了。

## Result

底下是 C_Chat 前 500MB 9grams 的結果：

![](https://i.imgur.com/Lb5Xb3t.png)

![](https://i.imgur.com/keSi67N.png)

Re:0 還是很多人在看嘛~ 之後第二多在討論的是聖人惠~XD

