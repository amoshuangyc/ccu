package main

import (
	"bufio"
	"fmt"
	"os"
)

func WordCount(inputPath, outputPath string) {
	fin, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}
	defer fin.Close()
	fout, err := os.Create(outputPath)
	if err != nil {
		panic(err)
	}
	defer fout.Close()

	scanner := bufio.NewScanner(fin)
	writer := bufio.NewWriter(fout)
	defer writer.Flush()

	writeItem := func(cur string, cnt int) {
		item := fmt.Sprintf("%020d %s", cnt, cur)
		writer.WriteString(item)
		writer.WriteString("\n")
	}

	cur := ""
	cnt := 0

	if scanner.Scan() {
		cur = scanner.Text()
		cnt = 1
	} else {
		return
	}

	for scanner.Scan() {
		nxt := scanner.Text()
		if cur == nxt {
			cnt++
		} else {
			writeItem(cur, cnt)
			cur = nxt
			cnt = 1
		}
	}

	writeItem(cur, cnt)
}
