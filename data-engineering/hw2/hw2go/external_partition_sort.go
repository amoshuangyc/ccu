package main

import (
	"bufio"
	"fmt"
	"os"
	"path"
	"sort"
	"strings"
	// "time"
)

func ExternalPartitionSort(inputPath, outputPath string, pivotItv int) {
	_ = os.Mkdir(path.Dir(chunkFmt), 0777)

	chunkCnt := partition(inputPath, pivotItv)
	sortAndConcatChunk(outputPath, chunkCnt)
	clearChunk(chunkCnt)

	// start := time.Now()
	// chunkCnt := partition(inputPath, pivotItv)
	// t1 := time.Since(start)
	// sortAndConcatChunk(outputPath, chunkCnt)
	// t2 := time.Since(start)
	// clearChunk(chunkCnt)
	// t3 := time.Since(start)

	// fmt.Println("chunkCnt", chunkCnt)
	// fmt.Println("parti:", t1)
	// fmt.Println("sort :", t2)
	// fmt.Println("clean:", t3)
}

func partition(inputPath string, pivotItv int) int {
	f, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}

	// find pivots by sampling every pivotItv lines
	pivots := make([]string, 0)
	pivotScanner := bufio.NewScanner(f)
	for ix := 0; pivotScanner.Scan(); ix++ {
		if ix%pivotItv == 0 {
			pivots = append(pivots, pivotScanner.Text())
		}
	}

	sort.Slice(pivots, func(i, j int) bool {
		return pivots[i] < pivots[j]
	})

	chunkCnt := len(pivots) + 1
	files := make([]*bufio.Writer, chunkCnt)
	for ix, _ := range files {
		chunkPath := fmt.Sprintf(chunkFmt, ix)
		chunkFile, err := os.Create(chunkPath)
		if err != nil {
			panic(err)
		}
		defer chunkFile.Close()
		files[ix] = bufio.NewWriter(chunkFile)
		defer files[ix].Flush()
	}

	f.Seek(0, 0)
	dataScanner := bufio.NewScanner(f)
	for dataScanner.Scan() {
		line := dataScanner.Text()
		ix := sort.Search(len(pivots), func(i int) bool {
			return pivots[i] >= line
		})

		files[ix].WriteString(line)
		files[ix].WriteString("\n")
	}

	return chunkCnt
}

func sortAndConcatChunk(outputPath string, chunkCnt int) {
	fout, err := os.Create(outputPath)
	if err != nil {
		panic(err)
	}
	defer fout.Close()

	writer := bufio.NewWriter(fout)
	defer writer.Flush()

	for ix := chunkCnt - 1; ix >= 0; ix-- {
		chunkPath := fmt.Sprintf(chunkFmt, ix)
		chunkFile, err := os.Open(chunkPath)
		if err != nil {
			panic(err)
		}
		scanner := bufio.NewScanner(chunkFile)

		data := make([]string, 0)
		for scanner.Scan() {
			data = append(data, scanner.Text())
		}

		if len(data) > 0 {
			sort.Slice(data, func(i, j int) bool {
				return data[i] > data[j]
			})
			writer.WriteString(strings.Join(data, "\n"))
			writer.WriteString("\n")
		}

		chunkFile.Close()
	}
}
