package main

import (
	"flag"
	"fmt"
	"os"
)

const (
	chunkFmt = "/tmp/hw2/%09d.chunk"
)

func main() {
	var inp = flag.String("i", "/dev/stdin", "input path")
	var out = flag.String("o", "/dev/stdout", "output path")
	var alg = flag.String("a", "merge", "algorithm")
	var par = flag.Int("p", 1000, "parameter")
	flag.Parse()

	inputPath := *inp
	outputPath := *out
	parameter := *par
	var extsort func(string, string, int)
	if *alg == "merge" {
		extsort = ExternalMergeSort
	} else {
		extsort = ExternalPartitionSort
	}

	for n := 4; n <= 9; n++ {
		tokens := fmt.Sprintf("/tmp/hw2/%dg.tokens", n)
		sorted := fmt.Sprintf("/tmp/hw2/%dg.sorted", n)
		counts := fmt.Sprintf("/tmp/hw2/%dg.counts", n)
		result := fmt.Sprintf("%s.%d", outputPath, n)

		fmt.Println("n =", n)

		fmt.Print("ngrams ...")
		Ngram(inputPath, tokens, n)
		fmt.Println("ok")
		
		fmt.Print("sort tokens ...")
		extsort(tokens, sorted, parameter)
		fmt.Println("ok")
		
		fmt.Print("word counts ...")
		WordCount(sorted, counts)
		fmt.Println("ok")
		
		fmt.Print("sort counts ...")
		extsort(counts, result, parameter)
		fmt.Println("ok")

		fmt.Println("-------------------")

		os.Remove(tokens)
		os.Remove(sorted)
		os.Remove(counts)
	}
}
