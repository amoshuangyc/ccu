package main

import (
	"bufio"
	"os"
)

func Ngram(inputPath, outputPath string, n int) {
	fin, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}
	defer fin.Close()
	scanner := bufio.NewScanner(fin)

	fout, err := os.Create(outputPath)
	if err != nil {
		panic(err)
	}
	defer fout.Close()
	writer := bufio.NewWriter(fout)
	defer writer.Flush()

	for scanner.Scan() {
		line := scanner.Text()
		deq := make([]rune, 0)
		for _, r := range line {
			if r < 256 {
				continue
			}
			deq = append(deq, r)

			if len(deq) == n {
				writer.WriteString(string(deq))
				writer.WriteString("\n")
				deq = deq[1:len(deq)]
			}
		}
	}
}
