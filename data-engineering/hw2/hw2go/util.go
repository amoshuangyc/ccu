package main

import (
	"fmt"
	"os"
)

func clearChunk(chunkCnt int) {
	for i := 0; i < chunkCnt; i++ {
		chunkPath := fmt.Sprintf(chunkFmt, i)
		err := os.Remove(chunkPath)
		if err != nil {
			panic(err)
		}
	}
}
