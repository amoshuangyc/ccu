package main

import (
	"bufio"
	"fmt"
	"os"
	"path"
	"sort"
	"strings"
	// "time"
)

func ExternalMergeSort(inputPath, outputPath string, chunkSize int) {
	_ = os.Mkdir(path.Dir(chunkFmt), 0777)

	chunkCnt := splitAndSort(inputPath, chunkSize)
	kWayMerge(outputPath, chunkCnt)
	clearChunk(chunkCnt)

	// start := time.Now()
	// chunkCnt := splitAndSort(inputPath, chunkSize)
	// t1 := time.Since(start)
	// kWayMerge(outputPath, chunkCnt)
	// t2 := time.Since(start)
	// clearChunk(chunkCnt)
	// t3 := time.Since(start)

	// fmt.Println("chunkCnt", chunkCnt)
	// fmt.Println("split:", t1)
	// fmt.Println("merge:", t2)
	// fmt.Println("clean:", t3)
}

func splitAndSort(inputPath string, chunkSize int) int {
	f, err := os.Open(inputPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	chunk := make([]string, 0)
	chunkCnt := 0

	flushChunk := func() {
		sort.Slice(chunk, func(i, j int) bool {
			return chunk[i] > chunk[j]
		})

		chunkPath := fmt.Sprintf(chunkFmt, chunkCnt)
		chunkFile, err := os.Create(chunkPath)
		if err != nil {
			panic(err)
		}
		chunkFile.WriteString(strings.Join(chunk, "\n"))
		chunkFile.Close()

		chunk = nil
		chunkCnt += 1
	}

	for scanner.Scan() {
		chunk = append(chunk, scanner.Text())
		if len(chunk) == chunkSize {
			flushChunk()
		}
	}
	if len(chunk) > 0 {
		flushChunk()
	}

	return chunkCnt
}

func kWayMerge(outputPath string, n int) {
	fout, err := os.Create(outputPath)
	if err != nil {
		panic(err)
	}
	defer fout.Close()
	writer := bufio.NewWriter(fout)
	defer writer.Flush()

	t := NewWinnerTree(n)
	defer t.Close()

	t.Build(0)

	for !t.Empty() {
		line := t.Pop()
		writer.WriteString(line)
		writer.WriteString("\n")
	}
}

type WinnerTree struct {
	n    int
	nn   int
	fs   []*os.File
	buf  []*bufio.Scanner
	tree []int
}

func NewWinnerTree(n int) *WinnerTree {
	w := new(WinnerTree)
	w.n = n
	w.nn = 1
	for w.nn < n {
		w.nn <<= 1
	}

	w.tree = make([]int, 2*w.nn-1)
	w.fs = make([]*os.File, n)
	w.buf = make([]*bufio.Scanner, n)
	for ix, _ := range w.buf {
		chunkPath := fmt.Sprintf(chunkFmt, ix)
		var err error
		w.fs[ix], err = os.Open(chunkPath)
		if err != nil {
			panic(err)
		}
		w.buf[ix] = bufio.NewScanner(w.fs[ix])
	}

	return w
}

func (w *WinnerTree) Build(u int) {
	if u >= w.nn-1 {
		ix := u - (w.nn - 1)
		if ix >= w.n || !w.buf[ix].Scan() {
			w.tree[u] = -1
		} else {
			w.tree[u] = ix
		}
		return
	}
	lch, rch := 2*u+1, 2*u+2
	w.Build(lch)
	w.Build(rch)
	switch {
	case w.tree[lch] == -1:
		w.tree[u] = w.tree[rch]
	case w.tree[rch] == -1:
		w.tree[u] = w.tree[lch]
	case w.buf[w.tree[lch]].Text() > w.buf[w.tree[rch]].Text():
		w.tree[u] = w.tree[lch]
	default:
		w.tree[u] = w.tree[rch]
	}
}

func (w *WinnerTree) Update(u int, val int) {
	if u >= w.nn-1 {
		ix := w.tree[u]
		if ix == -1 {
			return
		}
		if eof := !w.buf[ix].Scan(); eof {
			w.tree[u] = -1
		}
		return
	}
	lch, rch := 2*u+1, 2*u+2
	if w.tree[lch] == val {
		w.Update(lch, val)
	} else {
		w.Update(rch, val)
	}
	switch {
	case w.tree[lch] == -1:
		w.tree[u] = w.tree[rch]
	case w.tree[rch] == -1:
		w.tree[u] = w.tree[lch]
	case w.buf[w.tree[lch]].Text() > w.buf[w.tree[rch]].Text():
		w.tree[u] = w.tree[lch]
	default:
		w.tree[u] = w.tree[rch]
	}
}

func (w *WinnerTree) Empty() bool {
	return w.tree[0] == -1
}

func (w *WinnerTree) Pop() string {
	ix := w.tree[0]
	line := w.buf[ix].Text()
	w.Update(0, ix)
	return line
}

func (w *WinnerTree) Close() {
	for ix, _ := range w.fs {
		w.fs[ix].Close()
	}
}

func (w *WinnerTree) Pr(u int, ind int) {
	if u >= 2*w.nn-1 {
		return
	}

	ix := w.tree[u]
	for i := 0; i < ind; i++ {
		fmt.Print("   ")
	}
	if ix == -1 {
		fmt.Printf("%3d (xxx)\n", -1)
	} else {
		fmt.Printf("%3d (%3s)\n", ix, w.buf[ix].Text())
	}

	w.Pr(2*u+1, ind+1)
	w.Pr(2*u+2, ind+1)
}
