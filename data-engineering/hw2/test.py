import os
import time
import filecmp
import collections
from subprocess import run


def print_time(f):
    def time_func(*args, **kwargs):
        start_time = time.time()
        f(*args, **kwargs)
        end_time = time.time()
        print('time: {:.3f}'.format(end_time - start_time))
    return time_func


def ngram(data, n=4):
    for s in data:
        deq = collections.deque()
        for c in s:
            if ord(c) < 256:
                continue
            deq.append(c)
            if len(deq) == n:
                yield ''.join(deq)
                deq.popleft()

@print_time
def gen_ans(inp, out):
    with open(inp, 'r') as f:
        data = [line.rstrip() for line in f.readlines()]

    for n in range(4, 10):
        cnt = {}
        for token in ngram(data, n):
            if token in cnt:
                cnt[token] += 1
            else:
                cnt[token] = 1

        res = ['{:020d} {}\n'.format(v, k) for k, v in cnt.items()]
        res.sort(reverse=True)

        with open('{}.{}'.format(out, n), 'w') as f:
            f.writelines(res)

@print_time
def run_go(inp, out):
    cmd = './hw2go/hw2go -i {} -o {} -p 1024'.format(inp, out)
    run(cmd, shell=True)


def check(out, ans):
    for n in range(4, 10):
        po = '{}.{}'.format(out, n)
        pa = '{}.{}'.format(ans, n)
        print('{} == {} : {}'.format(po, pa, filecmp.cmp(po, pa)))
        os.remove(po)
        os.remove(pa)


if __name__ == '__main__':
    inp = './in'
    # inp = '../data/C_Chat_data.txt'
    out = './out'
    ans = './ans'
    run_go(inp, out)
    gen_ans(inp, ans)
    check(out, ans)